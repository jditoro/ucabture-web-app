import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';
import {Observable} from 'rxjs';

// interface Bcast {
//   title: String,
//   description: String,
//   groups: Array<String>,
//   username: String,
//   image: any
// }

@Injectable({
  providedIn: 'root'
})
export class DifundService {

  constructor(private http : HttpClient) { }

  public sendBroadcast(data : any) : Observable<any>{
    // console.log(data.values());
    // data = {content: data}

    console.log(data);
    return this.http.post(environment.apiUrl + 'admins/bcast', data, {
      reportProgress: true
    });
  }
}
