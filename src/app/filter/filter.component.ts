import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  public dateTime: Date;
  public active: boolean;
  public filters: number[];
  @Output() filterCheck: EventEmitter<any[]> = new  EventEmitter<any[]>();

  constructor() {
    this.active = false;
    this.filters = [];
  }

  ngOnInit() {
  }

  openFilters() {
    if ( !this.active ) {
      this.active = true;
    } else {
      console.log( this.filters);
      this.filterCheck.emit(this.filters);
    }
  }

  closeFilters() {
    this.active = false;
  }



  public pickEmoji(emoji: number) {
    if ( this.filters.indexOf(emoji) === -1) {
      this.filters.push(emoji);
    } else {
      this.filters.splice(this.filters.indexOf(emoji), 1);
    }
  }
}
