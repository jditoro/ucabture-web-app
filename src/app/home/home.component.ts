import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ModalService } from '../services/modal.service';
import { DifundComponent } from '../modals/difund/difund.component';
import { HomeService, MapData } from './home.service';
import { timer } from 'rxjs';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menu = {
    icon: 'menu'
  };

  public mapData: MapData;
  public mapDataCopy: MapData;
  public menuState: string;

  public filter: number[];
  public stats: number[];
  constructor(private modalService: ModalService, private homeService: HomeService, private router: Router) {
    this.filter = [];
    this.stats = [0, 0, 0, 0, 0, 0];
   }

  ngOnInit() {
    this.mapData = {} as MapData;
    this.homeService.mapData().subscribe(data => {
      this.mapData = data;
      this.mapDataCopy = data;
      this.stats = [0, 0, 0, 0, 0, 0];
      this.mapData.resume.forEach(element => {
        element.images.forEach( img => {
          // tslint:disable-next-line:radix
          this.stats[Number.parseInt(img.emoji) - 1]++;
        });
      });
      this.stats = this.stats.splice(0);
    });
  }

  logOut() {
    LoginService.logOut();
    this.router.navigateByUrl('/login');
  }

  displayMenu() {
    const menu: HTMLElement = document.querySelector('.menu') as HTMLElement;
    if (this.menu.icon === 'menu') {
      this.menu.icon = 'close';
      menu.classList.add('activate');
      document.querySelectorAll('.menu .menu-item').forEach(el => {
        el.classList.add('show');
      });
    } else {
      document.querySelectorAll('.menu .menu-item').forEach(el => {
        el.classList.remove('show');
      });
      menu.classList.remove('activate');
      this.menu.icon = 'menu';
    }
  }

  openDifundir() {
    this.modalService.init(
      DifundComponent,
      {},
      {}
    );
  }

  public useFilter(event: number[]) {
    console.log('filter');
    console.log(event);
    this.filter = [];
    timer(100).subscribe( x => {
      this.filter = event;
    });
    this.stats = [0, 0, 0, 0, 0, 0];
    this.mapData.resume.forEach(element => {
      element.images.forEach( img => {
        // tslint:disable-next-line:radix
        const n = Number.parseInt(img.emoji);
        if ( event.includes(n) || event.length === 0) {
          this.stats[n - 1]++;
        }
      });
    });
    this.stats = this.stats.splice(0);
  }
}

