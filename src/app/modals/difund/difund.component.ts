import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ModalService } from '../../services/modal.service';
import { DifundService } from './difund.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
// import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-difund',
  templateUrl: './difund.component.html',
  styleUrls: ['./difund.component.scss']
})
export class DifundComponent implements OnInit {

  public form: FormGroup;

  groups = [
    {value: 'estudiantes', title: 'Estudiantes'},
    {value: 'proftcompleto', title:'Prof. Tiempo Completo'},
    {value: 'proftconvencional', title: 'Prof. Tiempo Convencional'},
    {value: 'empleados', title: 'Empleados'},
    {value: 'egresados', title: 'Egresados'},
    {value: ['egresados','empleados', 'proftconvencional', 'proftcompleto', 'estudiantes'], title: 'Todos'}
  ];

  uploadImg: any;

  constructor(private modalService: ModalService, private difundService: DifundService, private fb: FormBuilder, private cd: ChangeDetectorRef, private snackBar: MatSnackBar) { 
    this.form = fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      groups: ['', Validators.required],
      image: ['', Validators.required],
      username: 'juandi'
    });

    // this.uploadImg = encodeURI("url('assets/upload_image.png')");
    this.uploadImg = "url('assets/upload_image.png')";
  }

  ngOnInit() {
  }

  onFileChange(event: Event | any) {
    const reader = new FileReader();
    const file = event.target.files[0];

    if (event.target.files && event.target.files.length) {
      reader.readAsDataURL(file);

      reader.onload = () => {
        // document.querySelector('.upload-image').style.backgroundImage = 'url('+reader.result+')';
      this.uploadImg = 'url('+reader.result+')';
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
    this.form.get('image').setValue(file);


    // this.post.image = event.target.files[0];
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Cerrar', {
      duration: 30000,
      panelClass: ['snack-bar']
    });
  }

  difuse() {
    console.log(this.form.value);

    if (this.form.invalid) {
      if (!this.form.get('image').valid) {
        this.openSnackBar('Imagen es requerida!');
      } else if (!this.form.get('title').valid) {
        this.openSnackBar('Titulo es requerido!');
      } else if (!this.form.get('description').valid) {
        this.openSnackBar('Descripcion es requerida!');
      }
      return;
    }

    const body = new FormData();
    body.append('title', this.form.get('title').value);
    body.append('image', this.form.get('image').value);
    body.append('description', this.form.get('description').value);
    body.append('groups', this.form.get('groups').value);
    body.append('username', this.form.get('username').value);

    this.openSnackBar('Enviando mensaje!');
    this.difundService.sendBroadcast(body).subscribe(((res) => {
      if (res.status === 'OK') {
        this.openSnackBar('Mensaje Difundido con exito!');
      } else {
        this.openSnackBar('Ha ocurrido un error, no hemos podimdo enviar el mensaje :(!');
      }
    }));

    this.close();
  }

  close() {
    this.modalService.destroy();
  }

}
