import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable,  } from '@angular/core';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { CircularBtnComponent } from './circular-btn/circular-btn.component';
import { FilterComponent } from './filter/filter.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { MatGridListModule } from '@angular/material/grid-list';
import { DetailedComponent } from './modals/detailed/detailed.component';
import { ModalService } from './services/modal.service';
import { DomService } from './services/dom.service';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { DifundComponent } from './modals/difund/difund.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { HistorialComponent } from './historial/historial.component';
import {Observable} from 'rxjs';

import {LoginService} from '../app/login/login.service';

@Injectable()
class CanActivateHome implements CanActivate {
  constructor(private router: Router) {}
 
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if(LoginService.isLogged()) return true;
    else {
      this.router.navigateByUrl('/');
      return false;
    }
  }
}

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [CanActivateHome] },
  { path: 'register', component: RegisterComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    CircularBtnComponent,
    FilterComponent,
    DetailedComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    StatisticsComponent,
    DifundComponent,
    HistorialComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { enableTracing: false }),
    BrowserModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SatDatepickerModule,
    SatNativeDateModule,
    MatGridListModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    MatSnackBarModule
    ],
  entryComponents: [
    DetailedComponent,
    DifundComponent
  ],
  exports: [
    CircularBtnComponent
  ],
  providers: [
    ModalService,
    DomService,
    CanActivateHome
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
