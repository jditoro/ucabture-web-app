import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-circular-btn',
  templateUrl: './circular-btn.component.html',
  styleUrls: ['./circular-btn.component.scss']
})
export class CircularBtnComponent {
  @Input() icon: string;
  @Input() class: string;
}
