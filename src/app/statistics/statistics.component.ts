import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  OnChanges,
  IterableDiffers,
  IterableDiffer,
  DoCheck,
  IterableChanges
} from '@angular/core';
import {
  Chart
} from 'chart.js';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit, OnChanges, DoCheck {

  chart: Chart;
  statsModal: any;
  emojis: any;
  colors: any;
  iterableDiffer: IterableDiffer<{}>;
  @Input() datos: any[];

  constructor(private _iterableDiffers: IterableDiffers) {
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
    this.emojis = [
      'Muy Feliz',
      'Feliz',
      'Neutral',
      'Incomodo',
      'Triste',
      'Molesto'
    ];
    this.datos = [1, 1, 1, 1, 1, 1];
    this.colors = ['rgba(104, 198, 46, 1)', 'rgba(219, 255, 57, 1)', 'rgba(255, 202, 40, 1)', 'rgba(255, 140, 40, 1)', 'rgba(225, 93, 49, 1)', 'rgba(230, 40, 40, 1)'];
  }

  ngDoCheck() {
    const changes: any = this.iterableDiffer.diff(this.datos);
    if (changes) {
      if (changes.collection !== undefined && changes.collection.length > 0) {
        this.chart.data.datasets.forEach((dataset, i) => {
          dataset.data = changes.collection;
        });
        this.chart.config.options.scales.yAxes[0].ticks.max = Math.max.apply(this, this.datos) + 5;
        console.log(this.chart);
        this.chart.update();
      }

    }
  }



  close() {
    const statsModal = document.querySelector('.statistics-container');
    statsModal.classList.remove('active');
    console.log('asd');
  }

  open() {
    const statsModal = document.querySelector('.statistics-container');
    statsModal.classList.add('active');
  }

  ngOnInit() {
    this.showChart();
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  showChart() {
    console.log(this.datos);
    this.chart = new Chart('bar_chart', {
      type: 'bar',
      data:{ 
        labels: this.emojis,
        datasets: [{
          label:'asd',
          data: this.datos,
          backgroundColor: this.colors,
          fill: true,
          lineTension: 0.2,
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          display: false,
          labels: {
          },

        },
        spanGaps: false,
        showLines: false,
        responsive: true,
        title: {
          text: 'Emociones vs Usuarios',
          display: false
        },
        scales: {

          // display: false,
          gridLines: {
            // display: false,
            lineWidth: 5,
            // drawBorder: false,
            // drawTicks: false,
            // drawOnChartArea: false,
            tickMarkLength: 0,
            // offsetGridLines: false,
          },
          yAxes: [{
            ticks: {
              min: 0,
              // max: 100,
              // max: Math.max.apply(this, this.datos) + 5,
              beginAtZero: true,
              display: false,
              lineHeight: 0,
              showLabelBackdrop: true,
              autoSkip: true

            },
            stacked: false
          }],
          xAxes: [{
            ticks: { display: false, min: 0, max: 6},
          }],
          ticks: {
            display: false
          }
        }
      }
    });
    Chart.plugins.register({
      afterDatasetsDraw: function (chart) {
        const ctx = chart.ctx;

        chart.data.datasets.forEach(function (dataset, i) {
          const meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function (element: any, index) {
              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgb(0, 0, 0)';

              const fontSize = 16;
              const fontStyle = 'normal';
              const fontFamily = 'Helvetica Neue';
              ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              const dataString = dataset.data[index].toString();

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';

              const padding = 5;
              const position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
            });
          }
        });
      }
    });
  }
}
