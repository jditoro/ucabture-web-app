import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircularBtnComponent } from './circular-btn.component';

describe('BtnComponent', () => {
  let component: CircularBtnComponent;
  let fixture: ComponentFixture<CircularBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircularBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircularBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
