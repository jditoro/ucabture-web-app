import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

interface LoginForm {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private static loggedUser: string;

  constructor(private http: HttpClient) {
  }

  public login(logForm: LoginForm): Observable<any> {
    const body = new URLSearchParams();
    body.set('username', logForm.username);
    body.set('password', logForm.password);
    console.log(body.toString());
    return this.http.post(environment.apiUrl + 'admins/login', body.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  public static setLoggedUser(user: string) {
    let expiracyDate = new Date();
    expiracyDate.setHours(expiracyDate.getHours() + 24);
    // window.localStorage.setItem('logged', 'true');
    window.localStorage.setItem('expiracy', expiracyDate.getDate().toString());
    this.loggedUser = user;
  }

  public static isLogged(): boolean {
    let expiracy = window.localStorage.getItem('expiracy');
    let expired = (new Date()).getDate() >= (new Date(expiracy)).getDate();
    if( expiracy && this.loggedUser == null ){
      window.localStorage.removeItem('expiracy');
      return false;
    } 
      
    return expiracy && !expired && this.loggedUser != null;
  }

  public static logOut() {
    this.loggedUser = null;
    window.localStorage.removeItem('expiracy');
  }

  public static getLoggedUser(): string{
    return this.loggedUser;
  }
}
