import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '../../../node_modules/@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public formulario: FormGroup;
  constructor(private fb: FormBuilder, private registerService: RegisterService, private router: Router, private snackBar: MatSnackBar) {
    this.formulario = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  public checkSignUp() {
    this.registerService.signUp(this.formulario.value).subscribe( res => {
      this.router.navigateByUrl('/login');
    }, err => {
      this.snackBar.open('Error al registrar usuario, intente de nuevo', "Ok");
    });
  }

}
