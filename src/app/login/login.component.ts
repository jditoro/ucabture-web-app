import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '../../../node_modules/@angular/router';
import { MatSnackBar } from '../../../node_modules/@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formulario: FormGroup;
  constructor(private fb: FormBuilder, private loginService: LoginService, private router: Router, private snackBar: MatSnackBar) {
    this.formulario = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    if(LoginService.isLogged()) {
      this.router.navigateByUrl('/home');
    }
  }

  public checkLogin() {
    if (this.formulario.invalid) {
      if(this.formulario.get('username').invalid) {
        this.snackBar.open('El campo Usuario no puede estar vacio', "Ok");
      } else if(this.formulario.get('password').invalid) {
        this.snackBar.open('El campo Contraseña no puede estar vacio', "Ok");
      }
      return;
    }
    console.log(this.formulario.value);
    this.loginService.login(this.formulario.value).subscribe( (res: Response) => {
      if(res.status === 200) {
        LoginService.setLoggedUser(this.formulario.value.username);
        this.router.navigateByUrl('/home');
      }
    }, (err) => {
      if(err.status === 404) {
        this.snackBar.open("El Usuario no existe", "Ok");
      } else if (err.status === 400) {
        this.snackBar.open("Clave Incorrecta", "Ok");
      }
    });
  }
}
