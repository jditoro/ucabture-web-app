import { ViewChild, Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
// import {google} from 'googlemaps';
import InfoBox from '../infobox/infobox';
import { ModalService } from '../services/modal.service';
import { DetailedComponent } from '../modals/detailed/detailed.component';
// import  _ from 'lodash';
import { Image, MapData } from '../home/home.service';

declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {

  @ViewChild('gmap')
  gmapElement: any;
  public map: google.maps.Map;
  @Input() data;
  @Input() filter: number[];
  geocoder: google.maps.Geocoder;
  res: any[];
  public markers: any[];
  public box: any[];
  public icons = [
    {
      url: 'assets/markers/marker1.png',
      scaledSize: new google.maps.Size(64, 64)
    },
    {
      url: 'assets/markers/marker2.png',
      scaledSize: new google.maps.Size(64, 64)
    },
    {
      url: 'assets/markers/marker3.png',
      scaledSize: new google.maps.Size(64, 64)
    },
    {
      url: 'assets/markers/marker4.png',
      scaledSize: new google.maps.Size(64, 64)
    },
    {
      url: 'assets/markers/marker5.png',
      scaledSize: new google.maps.Size(64, 64)
    },
    {
      url: 'assets/markers/marker6.png',
      scaledSize: new google.maps.Size(64, 64)
    }
  ];

  constructor(private modalService: ModalService) {
    this.res = [];
    this.markers = [];
    this.box = [];
    this.geocoder = new google.maps.Geocoder();
  }

  ngOnInit() {
    this.launchMap();
  }

  ngOnChanges(changes: SimpleChanges | any): void {
    console.log(changes);
    if (changes.filter !== undefined) {
      console.log(this.markers);
      this.markers.forEach((e, i) => {
        e.setMap(this.map);
        console.log( e);
      });
      console.log( this.markers);
      if (changes.filter.currentValue.length > 0) {
        for (let index = 0; index < this.markers.length; index++) {
          // console.log(this.markers[index]);
          // tslint:disable-next-line:radix
          if (!changes.filter.currentValue.includes(Number.parseInt(this.markers[index].info.emoji))) {
            this.deleteMarker(index);
          }
        }
      } else {
        console.log(this.markers);
        this.markers.forEach((e, i) => {
          e.setMap(this.map);
          console.log( e);
        });
        console.log( this.markers);

      }

    }
    if (changes.data !== undefined &&
      changes.data.currentValue !== undefined
      && changes.data.currentValue.resume !== undefined
      && changes.data.currentValue.resume.length > 0) {
      const mapData: MapData = changes.data.currentValue;

      mapData.resume.forEach(res => {
        if (res.images.length !== 0) {
          this.res.push(res.images);
        }
      });
      console.log('act');
      this.res.forEach(img => {
        // console.log(img);
        img.forEach(img2 => {
          // console.log(img2);
          this.addMarker(img2);
        });
      });
      // console.log(this.res);
    }

  }

  launchMap() {
    const mapProp = {
      center: new google.maps.LatLng(8.29674, -62.711653),
      zoom: 20,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };

    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    console.log(this.map);
    console.log(this.data);
    // Obtener de la api
    const res: Image[] = [
      {
        description: 'Increible vista de la Universidad Catolica Andres Bello, me encanta.',
        emoji: '1',
        lat: '8.296317',
        lng: '-62.71194',
        url: 'https://pbs.twimg.com/media/C5OXshNXUAQY7lL.jpg'
      },
      // {
      //   description: "Segunda imagen",
      //   emoji: 4,
      //   lat: 8.296869,
      //   lng: -62.711677,
      //   url:
      //     "http://guayanaweb.ucab.edu.ve/tl_files/relaciones_institucionales/otros/2015/Septiembre/IMGL4225.jpg"
      // },
      {
        description: 'Tercera imagen',
        emoji: '2',
        lat: '8.296693',
        lng: '-62.711554',
        url:
          'http://guayanaweb.ucab.edu.ve/tl_files/relaciones_institucionales/img/Foto%20Ucab%20Guayana%202012%20baja%20resol.jpg'
      },
      {
        description: 'Increible vista de la Universidad Catolica Andres Bello, me encanta.',
        emoji: '1',
        lat: '8.296713',
        lng: '-62.711375',
        url: 'http://farm3.static.flickr.com/2551/4118974150_cecd22a3a5.jpg'
      }
    ];

    this.res.push(res);
    // console.log(this.res);
    this.res.forEach(img => this.addMarker(img));
  }

  public addMarker(img: Image) {
    const latlng = { lat: parseFloat(img.lat), lng: parseFloat(img.lng) };

    const marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      icon: this.icons[parseFloat(img.emoji) - 1],
      map: this.map,
      info: {
        comment: img.description,
        emoji: img.emoji,
        img: img.url,
        place: 'No encontrado'
      }
    });


    const infobox = new InfoBox({
      content:
        '<div class="preview-container">' +
        '<img src="' + img.url + '" alt="" class="preview-img"> ' +
        '</div>',
      position: marker.position,
      disableAutoPan: true,
      pixelOffset: new google.maps.Size(-105, -225),
      closeBoxURL: '',
      isHidden: false
    });
    const that = this;
    this.geocoder.geocode({ location: latlng }, function (results, status) {
      // console.log('click event');
      if (results !== null && results[0]) {
        // console.log(results, status);
        marker.addListener('click', () => {
          // Mostrar ventana con detalle
          that.modalService.init(
            DetailedComponent,
            {
              comment: img.description,
              emoji: img.emoji,
              img: img.url,
              place: results[0].formatted_address
            },
            {}
          );
        });
      } else {
        marker.addListener('click', () => {
          // Mostrar ventana con detalle
          that.modalService.init(
            DetailedComponent,
            {
              comment: img.description,
              emoji: img.emoji,
              img: img.url,
              place: 'No encontrado'
            },
            {}
          );
        });
      }
    });


    marker.addListener('mouseover', () => {
      // Mostrar resumen
      infobox.open(this.map);
    });

    marker.addListener('mouseout', () => {
      infobox.close();
    });

    this.markers.push(marker);
    this.box.push(infobox);
    // console.log(this.markers);
  }



  public deleteMarker(index: number): void {
    this.markers[index].setMap(null);
  }

  public addMarkerAgain(index: number): void {
    this.markers[index].setMap(this.map);
  }

}
