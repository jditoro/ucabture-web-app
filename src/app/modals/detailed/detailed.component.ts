import { Component, OnInit, Input } from '@angular/core';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-detailed',
  templateUrl: './detailed.component.html',
  styleUrls: ['./detailed.component.scss']
})


export class DetailedComponent implements OnInit {

  // ID de la opinion
  // @Input('id') id: string;
  @Input() emoji: any;
  @Input() comment: string;
  @Input() img: string;

  public place;

  constructor(private modalService: ModalService) {
    console.log('Image ', this.img);

  }

  ngOnInit() {
    // Hacemos fetch de la informacion con el ID
  }

  searchEmoji(num: number) {
    return 'assets/emojis/' + num + '.png';
  }

  close() {
    this.modalService.destroy();
  }
}
