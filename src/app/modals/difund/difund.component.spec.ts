import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DifundComponent } from './difund.component';

describe('DifundComponent', () => {
  let component: DifundComponent;
  let fixture: ComponentFixture<DifundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DifundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DifundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
