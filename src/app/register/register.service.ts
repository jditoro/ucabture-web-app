import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

interface SignUpForm {
  name: string;
  lastname: string;
  username: string;
  password: string;
  email: string;
}

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  public signUp(signUpForm: SignUpForm): Observable<any> {
    const body = new URLSearchParams();
    body.set('username', signUpForm.username);
    body.set('password', signUpForm.password);
    body.set('name', signUpForm.name);
    body.set('lastname', signUpForm.lastname);
    body.set('email', signUpForm.email);
    console.log(body.toString());
  return this.http.post(environment.apiUrl + 'admins/signup', body.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }
}
